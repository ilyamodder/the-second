package ru.clienddev.thesecond;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.graphics.Insets;
import androidx.core.view.DisplayCutoutCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import ru.clienddev.thesecond.util.ResizeWidthAnimation;

public class MainActivity extends Activity {

    public final String HIGHSCORE_TAG = "highscore";
    public final float GOLDEN_RATIO = 0.618033988749895f;

    ViewGroup screen;
    TextView scoreView;
    TextView timerView;
    TextView textViewStartTip;
    TextView textViewRestartTip;
    TextView textViewHighscore;
    View bg2;
    ViewGroup bg;
    ImageView imageViewShare;

    float oldColor;

    Random random = new Random();
    SharedPreferences prefs;

    Timer timer;
    int time;
    int width;

    boolean isGameStarted = false;

    int score = 0;

    Runnable refreshTime = new Runnable() {
        @Override
        public void run() {
            timerView.setText(String.format("%02d:%02d:%02d", time / 1000 / 60, time / 1000 % 100 % 60, Math.round((double) time / 10.0) % 100));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        screen = (ViewGroup) findViewById(R.id.screen);
        scoreView = (TextView) findViewById(R.id.textView);
        timerView = (TextView) findViewById(R.id.timer);
        textViewStartTip = (TextView) findViewById(R.id.textViewStartTip);
        textViewRestartTip = (TextView) findViewById(R.id.textViewRestartTip);
        textViewHighscore = (TextView) findViewById(R.id.textViewHighscore);
        bg2 = findViewById(R.id.bg2);
        bg = (ViewGroup) findViewById(R.id.bg);
        imageViewShare = (ImageView) findViewById(R.id.imageViewShare);

        configureFullscreen();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;

        oldColor = random.nextFloat();

        prefs = PreferenceManager.getDefaultSharedPreferences(this);


        screen.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (!isGameStarted) newGame();
                    else if (((time % 1000 <= 10) && (time / 1000 == score + 1))
                            || ((time % 1000 >= 990) && (time / 1000 == score))) {
                        scoreView.setText(++score + "");

                    } else stopGame();
                    System.out.println(time);
                }
                return false;
            }
        });

        imageViewShare.setVisibility(View.GONE);
        imageViewShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Sharing", Toast.LENGTH_SHORT).show();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "I earned " + score + " points in \"The Second\"! Download it on Google Play: http://goo.gl/0saSDx");
                sendIntent.setType("text/plain");
                sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                MainActivity.this.startActivity(sendIntent);
            }
        });

        textViewRestartTip.setVisibility(View.GONE);

        refreshHighscore(prefs.getInt(HIGHSCORE_TAG, 0));
    }

    private void configureFullscreen() {
        WindowInsetsControllerCompat windowInsetsController =
                WindowCompat.getInsetsController(getWindow(), getWindow().getDecorView());
        // Configure the behavior of the hidden system bars.
        windowInsetsController.setSystemBarsBehavior(
                WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        );
        windowInsetsController.hide(WindowInsetsCompat.Type.systemBars());
        WindowCompat.setDecorFitsSystemWindows(getWindow(), false);

        ViewCompat.setOnApplyWindowInsetsListener(getWindow().getDecorView(), (v, windowInsets) -> {
            DisplayCutoutCompat cutout = windowInsets.getDisplayCutout();

            if (cutout == null) return WindowInsetsCompat.CONSUMED;

            ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) screen.getLayoutParams();
            mlp.leftMargin = cutout.getSafeInsetLeft();
            mlp.rightMargin = cutout.getSafeInsetRight();
            mlp.topMargin = cutout.getSafeInsetTop();
            mlp.bottomMargin = cutout.getSafeInsetBottom();
            screen.setLayoutParams(mlp);

            return WindowInsetsCompat.CONSUMED;
        });
    }

    public void newGame() {
        imageViewShare.setVisibility(View.GONE);
        time = 0;
        score = 0;
        scoreView.setText(0 + "");

        textViewHighscore.setVisibility(View.INVISIBLE);

        timer = new Timer();
        textViewStartTip.setVisibility(View.GONE);
        textViewRestartTip.setVisibility(View.GONE);
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                if (time % 10 == 0) runOnUiThread(refreshTime);
                if (time % 1000 == 0) runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ResizeWidthAnimation anim = new ResizeWidthAnimation(bg2, width);
                        anim.setDuration(1000);
                        if (isGameStarted) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //bg2.clearAnimation();
                                    float[] color = {oldColor * 360, 0.5F, 0.75F};
                                    bg.setBackgroundColor(Color.HSVToColor(color));
                                    oldColor += GOLDEN_RATIO;
                                    if (oldColor > 1) oldColor -= 1;

                                    color[0] = oldColor * 360;
                                    bg2.setBackgroundColor(Color.HSVToColor(color));
                                }
                            }, 5);
                            bg2.startAnimation(anim);
                        }
                        //bg2.setBackgroundColor(Color.);
                    }
                });
                time++;
            }
        }, 1, 1);
        isGameStarted = true;
    }

    public void stopGame() {
        try {
            timer.cancel();
        } catch (Exception e) {

        }
        textViewStartTip.setVisibility(View.GONE);
        textViewRestartTip.setVisibility(View.VISIBLE);
        isGameStarted = false;
        bg2.clearAnimation();
        updateHighscore(score);
        textViewHighscore.setVisibility(View.VISIBLE);
        imageViewShare.setVisibility(View.VISIBLE);
        System.out.println(time);
        refreshTime.run();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopGame();
    }

    public void updateHighscore(int score) {
        int highscore = prefs.getInt(HIGHSCORE_TAG, 0);
        if (score > highscore) {
            prefs.edit().putInt(HIGHSCORE_TAG, score).commit();
            refreshHighscore(score);
        } else {
            refreshHighscore(highscore);
        }
    }

    public void refreshHighscore(int score) {
        textViewHighscore.setText(String.format(getString(R.string.highscore_message), score));
    }
}
